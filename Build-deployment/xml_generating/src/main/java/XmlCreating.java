import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
public class XmlCreating {

    public static void main(String[] args) throws Exception {
        VelocityEngine ve = new VelocityEngine();
        ve.init();

        ArrayList list = new ArrayList();
        Map map = new HashMap();
        map.put("name", "horse");
        map.put("price", "00.00");
        list.add( map );

        map = new HashMap();
        map.put("name", "dog");
        map.put("price", "9.99");
        list.add( map );
        map = new HashMap();
        map.put("name", "bear");
        map.put("price", ".99");
        list.add( map );
        try {


            VelocityContext context = new VelocityContext();
            context.put("petList", list);
            Template t = ve.getTemplate("src/helloworld.vm");
            StringWriter writer = new StringWriter();
            t.merge(context, writer);
            System.out.println(writer.toString());
        } catch (ResourceNotFoundException rnfe) {
            System.out.println("Couldn't find the template");
        } catch (ParseErrorException pee) {
            System.out.println("syntax error: problem parsing the template");
        } catch (MethodInvocationException mie) {
            System.out.println("something invoked in the template");
            System.out.println("threw an exception");
        } catch (Exception e) {
        }
    }
}
