from __future__ import absolute_import, print_function, unicode_literals
import sys
import requests
import json
import random
import re
import pandas as pd
from bs4 import BeautifulSoup
import logging


logging.basicConfig(filename='pytrends.log',level=logging.DEBUG)

class TrendReq(object):
    """
    Google Trends API
    """

    def __init__(self, username, password, custom_useragent=None):
        """
        Initialize hard-coded URLs, HTTP headers, and login parameters
        needed to connect to Google Trends, then connect.
        """
        self.username = username
        self.password = password
        # google rate limit
        self.google_rl = 'You have reached your quota limit. Please try again later.'
        self.url_login = "https://accounts.google.com/ServiceLogin"
        self.url_auth = "https://accounts.google.com/ServiceLoginAuth"
        # custom user agent so users know what "new account signin for Google" is
        if custom_useragent is None:
            self.custom_useragent = {'User-Agent': 'PyTrends'}
        else:
            self.custom_useragent = {'User-Agent': custom_useragent}
        self._connect()
        self.results = None

       

    def _connect(self):
        """
        Connect to Google.
        Go to login page GALX hidden input value and send it back to google + login and password.
        http://stackoverflow.com/questions/6754709/logging-in-to-google-using-python
        """
        self.ses = requests.session()
        login_html = self.ses.get(self.url_login, headers=self.custom_useragent)
        soup_login = BeautifulSoup(login_html.content, "lxml").find('form').find_all('input')
        dico = {}
        for u in soup_login:
            if u.has_attr('value'):
                dico[u['name']] = u['value']
        # override the inputs with out login and pwd:
        dico['Email'] = self.username
        dico['Passwd'] = self.password
        self.ses.post(self.url_auth, data=dico)

    def LoadUserAgents(self, uafile):
        """
        uafile : string
            path to text file of user agents, one per line
        """
        uas = []
        with open(uafile, 'rb') as uaf:
            for ua in uaf.readlines():
                if ua:
                    uas.append(ua.strip()[1:-1 - 1])
        random.shuffle(uas)
        return uas

    def trend(self, payload, proxy, return_type=None):
        payload['cid'] = 'TIMESERIES_GRAPH_0'
        payload['export'] = 3
        req_url = "https://www.google.com/trends/fetchComponent"

        user_agents = self.LoadUserAgents(uafile="user_agent.txt")

        ua = random.choice(user_agents)
        #local_proxy = random.choice(self.proxy)
        #print(local_proxy)

        header = {
            "Connection": "close",  # another way to cover tracks
            "User-Agent": ua}

        req = self.ses.get(req_url, proxies = proxy, timeout = 60, params = payload, headers = header)
        try:
            if self.google_rl in req.text:
                raise RateLimitError
            # strip off js function call 'google.visualization.Query.setResponse();
            text = req.text[62:-2]
            # replace series of commas ',,,,'
            text = re.sub(',+', ',', text)
            # replace js new Date(YYYY, M, 1) calls with ISO 8601 date as string
            pattern = re.compile(r'new Date\(\d{4},\d{1,2},\d{1,2}\)')
            for match in re.finditer(pattern, text):
                # slice off 'new Date(' and ')' and split by comma
                csv_date = match.group(0)[9:-1].split(',')
                year = csv_date[0]
                # js date function is 0 based... why...
                month = str(int(csv_date[1]) + 1).zfill(2)
                day = csv_date[2].zfill(2)
                # covert into "YYYY-MM-DD" including quotes
                str_dt = '"' + year + '-' + month + '-' + day + '"'
                text = text.replace(match.group(0), str_dt)
                req.close()
            self.results = json.loads(text)
        except ValueError:
            raise ResponseError(req.content)
        if return_type == 'json' or return_type is None:
            return self.results
        if return_type == 'dataframe':
            self._trend_dataframe()
            return self.results

    def trend_Single(self, url):

        print(url)
        content = requests.get(url)
        content = content.text[62:-2]
        content = re.sub(',+', ',', content)
        pattern = re.compile(r'new Date\(\d{4},\d{1,2},\d{1,2}\)')
        for match in re.finditer(pattern, content):
            # slice off 'new Date(' and ')' and split by comma
            csv_date = match.group(0)[9:-1].split(',')
            year = csv_date[0]
            # js date function is 0 based... why...
            month = str(int(csv_date[1]) + 1).zfill(2)
            day = csv_date[2].zfill(2)
            # covert into "YYYY-MM-DD" including quotes
            str_dt = '"' + year + '-' + month + '-' + day + '"'
            content = content.replace(match.group(0), str_dt)
        self.results = json.loads(content)
        self._trend_dataframe()
        return self.results

    def _trend_dataframe(self):
        # Only for trends
        df = pd.DataFrame()
        headers = []
        for col in self.results['table']['cols']:
            headers.append(col['label'])
        for row in self.results['table']['rows']:
            row_dict = {}
            for i, value in enumerate(row['c']):
                row_dict[headers[i]] = value['v']
            df = df.append(row_dict, ignore_index=True)
        df['Date'] = pd.to_datetime(df['Date'])
        df.set_index('Date', inplace=True)
        self.results = df
        return self.results


class Error(Exception):
    """Base class for exceptions in this module."""
    pass


class RateLimitError(Error):
    """Exception raised for exceeding rate limit"""

    def __init__(self):
        self.message = "Exceeded Google's Rate Limit. Please use time.sleep() to space requests."
        print(self.message)


class ResponseError(Error):
    """Exception raised for exceeding rate limit"""

    def __init__(self, content):
        self.message = "Response did not parse. See server response for details."
        self.server_error = BeautifulSoup(content, "lxml").findAll("div", {"class": "errorSubTitle"})[0].get_text()
        print(self.message)
        print(self.server_error)

