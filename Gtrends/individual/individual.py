from pytrends.request import TrendReq
import json
import time
from datetime import datetime
import random
import logging
import pandas as pd
from sys import argv
import configparser
logging.basicConfig(filename='example.log', level=logging.DEBUG)

idx = 0
all_dfs = {}
Config = configparser.ConfigParser()
Config.read(argv[1])
Config.sections()
CsvPath=Config.get('SectionOne', 'LocalPath1')
with open('/home/ec2-user/code/final_input.txt') as f:
	next(f)
	for val in f:
	
		# pytrend = TrendReq("js970008@gmail.com", "durham19", custom_useragent='My Pytrends Script')
		# pytrend = TrendReq("pytrendskohls2@gmail.com", "zjgdsg588", custom_useragent='My Pytrends Script')
		pytrend = TrendReq("gttest.22@gmail.com", "ABCdef01", custom_useragent='ABC')
		# time.sleep(random.randint(5,10))
		time.sleep(6)
		theme, category, data = val.rstrip().split(',')[:3]
    	#print ("theme = %s, category = %s, search = %s" % (instance, ts, data))
		trend_payload = {'q': [data], 'date': 'today 189-d'}  # 36-m
		df = pytrend.trend(trend_payload, return_type='dataframe')
		df.insert(1,'theme', pd.Series([theme for x in range(len(df.index))], index=df.index))
		df.insert(2, 'category',pd.Series([category for x in range(len(df.index))], index=df.index))
		df.insert(3, 'keyword',pd.Series([data for x in range(len(df.index))], index=df.index))
		df.insert(4, 'TimeStamp', pd.Series(datetime.strftime(datetime.now(), '%Y-%m-%d'), index=df.index))
		df.to_csv(CsvPath+str(idx) + ".csv")
		idx = idx + 1
		print (idx, data)
		logging.info ('Sample Output')
	print ("Done")
