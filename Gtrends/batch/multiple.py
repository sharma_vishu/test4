from  pytrends.request import TrendReq
from itertools import permutations
import numpy as np
import pandas as pd
import configparser
from sys import argv
#import matplotlib.pyplot as plt
import json
import time
import pickle
import random
import datetime

#accounts = ['pytrendskohls', 'pytrendskohls0', 'pytrendskohls1', 'pytrendskohls2', 'pytrendskohls3', 'pytrendskohls4']
#password = 'zjgdsg588'

#keywords = ['black jeans', 'leather skinny jeans', 'waxed jeans', 'coated jeans', 'printed jeans', 'colored jeans', 'leather jeans', 'white jeans', 'embellished jeans', 'distressed jeans']
#Config = configparser.ConfigParser()
#Config.read(argv[1])
#Config.sections()
#CsvPath=Config.get('SectionOne', 'LocalPath2')
CsvPath='/home/vishunath/Shared/Gtrends/batch/'
df = pd.read_csv('/home/vishunath/Shared/Gtrends/batch/batch9.csv')
print(df)
#print(df['1'])
#print("1 ", list(df['Keyword']))
print("2 ",list(df.ix[:, 2]))

	

#keywords = ['black jeans', 'leather skinny jeans', 'waxed jeans', 'coated jeans']

keywords = list(df.ix[:, 2])

print("keywords ", keywords)


all_pairs = list(permutations(keywords, 2))

i = 0

temp_pairs = [tuple(sorted(pair)) for pair in all_pairs]
print ("temp_pairs ", temp_pairs)
all_pairs = list(set(temp_pairs))
print("all_pairs ", all_pairs)

average_ratios = {}

# start index if API calls is limited with 45 calls.
start = 0
i = start
j = start


# proxy_1 = {'http_1', 'http://198.178.126.175:3128'}
# proxy_2 = {'http_2', 'http://69.46.134.115:80'}
# proxy_3 = {'http_3', 'http://107.182.111.27:8080'}

#proxy = [proxy_1, proxy_2,proxy_3,proxy_4,proxy_5,proxy_6,proxy_7]


pytrend = TrendReq("vishudadhich001@gmail.com", "427744AZ@")
#pytrend = TrendReq("pytrendskohls2@gmail.com", "zjgdsg588", custom_useragent='My Pytrends Script')

all_dfs = {}

for pair in all_pairs[start:]:
	#if i != 0 and i % 5 == 0:
	#local_proxy = random.choice(list(proxy))
	#time.sleep(random.randint(5,10))
	time.sleep(15)
	keyword = ','.join(pair)
	print ("index: ", i)
	i += 1
	try:
		df = pytrend.trend({'q': keyword, 'date': 'today 189-d'},return_type='dataframe')
		dates = np.asarray(df.index.strftime("%Y-%m-%d"))
		#print("dates ",dates)
		df.index = dates

		# if API calls is unlimited for 45 calls. Then store df into dictionary directly without dumping into local.
		all_dfs[tuple(df.columns.values.tolist())] = df
		print("tuple(df.columns.values.tolist()) ",tuple(df.columns.values.tolist()))
		print("all_dfs ", all_dfs)
	except ValueError:
	#	type, value, traceback = sys.exc_info()
		print("data are not available")
	except IndexError:
	   print("list index out of range")
	#except:
		#print("exception has been handled")	
	
	# if API calls is limited within 45 calls. Then uncommented this to store data to local.
	# pickle.dump(df, open("data_pytrends_jeans_new/temp" + str(j) + ".p", "wb"))
	# j += 1

sets = set(all_pairs)


# if API calls is limited within 45 calls. Then uncommented this to load data from local.
# for i in range(45):
# 	df = pickle.load(open("data_pytrends_jeans_new/temp" + str(i) + ".p", "rb"))
# 	all_dfs[tuple(df.columns.values.tolist())] = df

################################### Compute Average Ratios and Ranking Keywords according to # of their ratios less than 1 ##############################################################
average_ratios = {}
# average_ratios is stored in both direction. e.g. if k1 / k2 = 0.5. Then I store average_ratios[k1][k2] = 0.5 and average_ratios[k2][k1] = 2
for pair in all_pairs:
	df = all_dfs[pair]
	
	first = pair[0]
	second = pair[1]
	print("first & second ", first, second)

	average_ratio_0 = np.mean(df.apply(lambda x: np.divide(x[0] * 1.0, x[1]), axis=1).values)
	average_ratio_1 = np.mean(df.apply(lambda x: np.divide(x[1] * 1.0, x[0]), axis=1).values)
	print("average_ratio_1 ", average_ratio_1)
	print("average_ratio_1 ", average_ratio_1)
	
	
	if first not in average_ratios.keys():
		average_ratios[first] = dict()

	if second not in average_ratios.keys():
		average_ratios[second] = dict()

	average_ratios[first][second] = average_ratio_0
	average_ratios[second][first] = average_ratio_1

# Compute # of ratios less than 1 for each keyword.
times_keywords_ratio_less_than_one = []
for keyword in keywords:
	all_ratios = list(average_ratios[keyword].values())
	times_keywords_ratio_less_than_one.append([keyword, sum([1 for r in all_ratios if r < 1])])

times_keywords_ratio_less_than_one = sorted(times_keywords_ratio_less_than_one, key=lambda x: x[1], reverse=True)

############################################# Renormalization ############################################################
all_pairs = list(all_dfs.keys())
new_keywords = [k[0] for k in times_keywords_ratio_less_than_one]
print("new_keywords ", new_keywords)
j = 2

result = pd.DataFrame(None)
final_result = pd.DataFrame(None)
dates = list(all_dfs.values())[0].index
print("result 1 --",result)

while j < 15:  #hardcoded this size.
	lowest_keyword = new_keywords[j-2]
	second_lowest_keyword = new_keywords[j-1]
	third_lowest_keyword = new_keywords[j]

	print (lowest_keyword, second_lowest_keyword, third_lowest_keyword)

	pair_0 = tuple(sorted([lowest_keyword, second_lowest_keyword]))
	pair_1 = tuple(sorted([second_lowest_keyword, third_lowest_keyword]))
	pair_2 = tuple(sorted([lowest_keyword, third_lowest_keyword]))

	temp_df_0 = all_dfs[pair_0]
	temp_df_1 = all_dfs[pair_1]
	temp_df_2 = all_dfs[pair_2]
	print("0 ",temp_df_0)
	print("1 ",temp_df_1)
	print("2 ",temp_df_2)

	if j == 2:
		result = pd.concat([result, temp_df_0], axis=1)
		result.index = dates
		print("result 2--",result)


	arr_0 = temp_df_0[second_lowest_keyword].values
	arr_1 = temp_df_1[second_lowest_keyword].values
	print("arr_0 ", arr_0)
	print("arr_1 ", arr_1)


	ratios = arr_0 / arr_1
	print (ratios)
      
      

	# Renormalize lower ranking keywords by the ratios between the overlap keyword between result and df for the pair (overlap keyword, higher ranking keyword)
	# Then concat with higher ranking keyword.
	result = result.apply(lambda x: x / ratios, axis=0)
	result = result.drop(second_lowest_keyword, 1)
	result = pd.concat([result, temp_df_1], axis=1)
	j += 1
	print("result 3 --",result)

# Output csv file and plot
#result = result[['Year', 'quarter']].apply(lambda x: ''.join(x), axis=1)
#result = result.apply(lambda x: '*'.join(x.dropna().astype(str).values), axis=1)
#final_result = result.ix[:, 0]
#arr = sparse.coo_matrix((result.ix[:, ], (result.ix[:, 0], result.ix[:, 0])), shape=(3,3))
#final_result =  

result.to_csv(CsvPath+"Bottoms_Silhouette_normalized.csv", ",")
#result['dates'] = dates
#result.plot(x='dates')
#plt.show()






















