import requests
#import gTrends
import configparser
from sys import argv
import sys
import json
import re
import pandas as pd
from bs4 import BeautifulSoup

###function
def trend_dataframe(results):
        # Only for trends
        df = pd.DataFrame()
        headers = []
        for col in results['table']['cols']:
            headers.append(col['label'])
        for row in results['table']['rows']:
            row_dict = {}
            for i, value in enumerate(row['c']):
                row_dict[headers[i]] = value['v']
            df = df.append(row_dict, ignore_index=True)
        df['Date'] = pd.to_datetime(df['Date'])
        df.set_index('Date', inplace=True)
        results = df
        return results

##code

Config = configparser.ConfigParser()
Config.read(argv[1])
Config.sections()
CsvPath=Config.get('SectionOne', 'LocalPath')
url="https://www.google.com/trends/fetchComponent?hl=en-US&q=colored%20jeans&geo=US&cid=TIMESERIES_GRAPH_0&export=3&date=today%20189-d"
content=requests.get(url)
content = content.text[62:-2]
content = re.sub(',+', ',', content)
pattern = re.compile(r'new Date\(\d{4},\d{1,2},\d{1,2}\)')
for match in re.finditer(pattern, content):
                # slice off 'new Date(' and ')' and split by comma
                csv_date = match.group(0)[9:-1].split(',')
                year = csv_date[0]
                # js date function is 0 based... why...
                month = str(int(csv_date[1]) + 1).zfill(2)
                day = csv_date[2].zfill(2)
                # covert into "YYYY-MM-DD" including quotes
                str_dt = '"' + year + '-' + month + '-' + day + '"'
                content = content.replace(match.group(0), str_dt)
results = json.loads(content)
results_df = trend_dataframe(results)
print (CsvPath)
results_df.to_csv(CsvPath+"test.csv")
print (results_df)



#soup=BeautifulSoup(content.text,'html.parser')
#print (soup.get_text().encode("utf-8"))
